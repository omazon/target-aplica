import Vue from 'vue'
import App from './App.vue'
// VALIDADOR
import es from 'vee-validate/dist/locale/es' ;
import VeeValidate,{Validator} from 'vee-validate';
//MÁSCARA
import AwesomeMask from 'awesome-mask'
//AXIOS
window.axios = require ('axios');
//MODAL
import VueSweetAlert from 'vue-sweetalert';
//SUBIR ARCHIVOS
import VueClip from 'vue-clip';
//CAPTCHA
import VueRecaptcha from 'vue-recaptcha';

Vue.directive('mask',AwesomeMask);
Vue.use(VueSweetAlert);
Vue.use(VueClip);
Vue.component('vue-recaptcha',VueRecaptcha);
// Vue.component('file-upload',FileUpload);
Validator.addLocale(es);
Vue.use(VeeValidate,{
    locale:'es'
});
new Vue({
  el: '#app',
  render: h => h(App),
});
